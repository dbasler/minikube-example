FROM golang:alpine AS builder

RUN apk add --no-cache git

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin

WORKDIR $GOPATH/src/minikube
COPY . .

RUN go mod tidy

RUN go build -o /go/bin/minikube

FROM scratch

# Copy our static executable.
COPY --from=builder /go/bin/minikube /go/bin/minikube

ENTRYPOINT ["/go/bin/minikube"]