<https://www.digitalocean.com/community/tutorials/how-to-make-an-http-server-in-go>

<https://chemidy.medium.com/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324>

<https://minikube.sigs.k8s.io/docs/start/>

<https://kubernetes.io/docs/reference/kubectl/cheatsheet/>

### To create an image

```docker
docker build -t 19841981/minikube . && docker run -p 8080:8080 -it 19841981/minikube
```

### To run the deployed cluster

```shell
kubectl port-forward service/hello-minikube 8080:8080
```

### Call the root endpoint

```shell
curl --header "Content-Type: application/json" --request POST --data '{"name":"Darren","age":42}' "http://localhost:8080?first=D&second=B"
```

### Call the hello endpoint

```shell
curl --header "Content-Type: application/json" --request POST --data '{"name":"Darren","age":42}' http://localhost:8080/hello
```
